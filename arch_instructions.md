# Arch Linux Personal Install commands
Verify the boot mode:  
`ls /sys/firmware/efi/efivars`  
## Network
### For Laptop
`iwctl`  
`station list`  
`station wlan0 scan`  
`station wlan0 get-networks`  
`station wlan0 connect <NETWORK-SSID>`  
## Continuing on...
Test connection:  
`ping archlinux.org`  
Ensure system clock is accurate:  
`timedatectl set-ntp true`  
`timedatectl status`  
## Disk
`fdisk -l`  
`fdisk /dev/sda` (Or the disk to be partitioned)  
`FDISK $ g`  
`FDISK $ n`  
Default twice  
`FDISK $ +520M`  
If it asks to remove signature, say yes  
`FDISK $ n`  
Default twice  
`FDISK $ +4G` (Twice the amount of physical RAM)  
`FDISK $ n`  
Default thrice  
`FDISK $ t`  
`FDISK $ 1`  
`FDISK $ uefi`  
`FDISK $ t`  
`FDISK $ 2`  
`FDISK $ swap`  
`FDISK $ t`  
Default  
`FDISK $ 23`  
`FDISK $ w`  
  
`mkfs.fat -F 32 /dev/sda1`  
`mkswap /dev/sda2`  
`mkfs.ext4 /dev/sda3`  
  
`mount /dev/sda3 /mnt`  
`mkdir -p /mnt/boot`  
`mount /dev/sda1 /mnt/boot`  
`swapon /dev/sda2`  
  
## Installing Linux and the system
`pacstrap /mnt base linux-lts linux-firmware`  
  
`genfstab -U /mnt >> /mnt/etc/fstab`  
## Chroot
`arch-chroot /mnt`  
`pacman -S nano vim networkmanager nm-connection-editor network-manager-applet xorg xorg-server`  
THEN ADD  
`gnome gnome-extra gnome-shell-extensions`  
OR  
`xfce4 xfce4-goodies lightdm lightdm-gtk-greeter xorg-server`  
OR  
`plasma-meta kde-applications plasma-wayland-session egl-wayland nvidia-lts` (egl-wayland for NVIDIA only)  
EXTRA  
`htop neofetch sudo`  
### XFCE
`systemctl enable lightdm` OR  
### GNOME
`systemctl enable gdm`  
  
---
  
`systemctl enable NetworkManager`  
  
`EDITOR=nano visudo`  
Uncomment wheel or sudo group, use Ctrl-W  
  
`useradd -m iason`  
`usermod -aG wheel iason`  
`passwd iason`  
  
`ln -sf /usr/share/zoneinfo/NZ /etc/localtime`  
`hwclock --systohc`  
  
`nano /etc/locale.gen`  
Ctrl-W to find `en_US.UTF-8` then uncomment it  
`locale-gen`  
`echo "LANG=en_US.UTF-8" > /etc/locale.conf`  
`echo arcadia > /etc/hostname`  
`nano /etc/hosts`  
Add the following:  
```  
127.0.0.1	localhost  
::1		    localhost  
127.0.1.1	arcadia.greece arcadia  
```  
  
Set the root password:  
`passwd`  
## Bootloader
`pacman -S grub efibootmgr os-prober intel-ucode` (or amd-ucode depending on your processor)  
`grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB`  
`efibootmgr`  
`nano /etc/default/grub`  
Add this to the end of the file: `GRUB_DISABLE_OS_PROBER=false`  
`grub-mkconfig -o /boot/grub/grub.cfg`  
## Finally
Exit the chroot environment with Ctrl-D  
`umount -R /mnt`  

## Afterwords
Install yay:  
```
sudo pacman -S base-devel git
cd /opt
sudo git clone https://aur.archlinux.org/yay.git
sudo chown -R <username>:users ./yay
cd yay
makepkg -si
```
Once you've got yay installed, to supress warnings, install this:  
`yay -S aic94xx-firmware wd719x-firmware upd72020x-fw`
