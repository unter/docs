# Firefox Setup
What to do when first setting up Firefox.
## Addons
Install these addons:  
* [UBlock Origin](https://addons.mozilla.org/en-GB/firefox/addon/ublock-origin/)
* [Decentraleyes](https://addons.mozilla.org/en-GB/firefox/addon/decentraleyes/)
* [HTTPS Everywhere](https://addons.mozilla.org/en-GB/firefox/addon/https-everywhere/)
* [ClearURLs](https://addons.mozilla.org/en-US/firefox/addon/clearurls/)
## Options
* Set the default search engine to DuckDuckGo

## about:config
In the about:config, set these options.  
`media.peerconnection.enabled` to `false` - This disables WebRTC leaks  
To be extra sure, ensure these options are set:  
* `media.peerconnection.turn.disable` is `false`
* `media.peerconnection.use_document_iceservers` is `true`
* `media.peerconnection.video.enabled` is `true`
* `media.peerconnection.identity.timeout` is `10000`
### Firefox: Privacy Related "about:config" Tweaks from https://privacytools.io
Set these options:  
**privacy.firstparty.isolate = true**  
**privacy.resistFingerprinting = true**  
**privacy.trackingprotection.fingerprinting.enabled = true**  
**privacy.trackingprotection.cryptomining.enabled = true**  
**privacy.trackingprotection.enabled = true**  
**browser.send_pings = false**  
**browser.urlbar.speculativeConnect.enabled = false**  
**dom.event.clipboardevents.enabled = false**  
**media.eme.enabled = false**  
Disables playback of DRM-controlled HTML5 content.  
**media.gmp-widevinecdm.enabled = false**  
**media.navigator.enabled = false**  
**network.cookie.cookieBehavior = 1**  
Disable cookies  
* 0 = Accept all cookies by default
* 1 = Only acept from the originating site (block third-party cookies)
* 2 = Block all cookies by default
**network.http.referer.XOriginPolicy = 2**  
Only send `Referer` header when the full hostnames match.  
* 0 = Send `Referer` in all cases
* 1 = Send `Referer` to same eTLD sites
* 2 = Send `Referer` only when the full hostnames match
**network.http.referer.XOriginTrimmingPolicy = 2**  
When sending `Referer` across origins, only send scheme, host, and port in the Referer header of cross-origin requests.  
* 0 = Send full url in `Referer`
* 1 = Send url without query string in `Referer`
* 2 = Only send scheme, host, and port in `Referer`
**webgl.disabled = true**  
**browser.sessionstore.privacy_level = 2**  
This preference controls when to store extra information about a session: contents of forms, scrollbar positions, cookies, and POST data.
* 0 = Store extra session date for any site. (Default starting with Firefox 4.)
* 1 = Store extra session data for unencrypted (non-HTTPS) sites only. (Default before Firefox 4.)
* 2 = Never store extra session data.
**beacon.enabled = false**  
**browser.safebrowsing.downloads.remote.enabled = false**  
**network.IDN_show_punycode = true**  
#### Disable Firefox prefetching pages it thinks you will visit next:
**network.dns.disablePrefetch = true**  
**network.dns.disablePrefetchFromHTTPS = true**  
**network.predictor.enabled = false**  
**network.predictor.enable-prefetch = false**  
**network.prefetch-next = false**  

